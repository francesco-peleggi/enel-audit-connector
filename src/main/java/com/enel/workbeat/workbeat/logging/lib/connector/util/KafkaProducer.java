package com.enel.workbeat.workbeat.logging.lib.connector.util;

import com.enel.workbeat.workbeat.logging.lib.connector.config.KafkaConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.kafka.core.KafkaTemplate;

public class KafkaProducer {

    private static final Logger log = LoggerFactory.getLogger(KafkaProducer.class);

    @Autowired
    @Qualifier("nodeEventProducer")
    private KafkaTemplate<String,String> nodeTemplate;

    @Autowired
    @Qualifier("taskEventProducer")
    private KafkaTemplate<String,String> taskTemplate;

    @Autowired
    private KafkaConfiguration kafkaConfig;

    public void sendMessage(WorkbeatAuditConst.EventType eventType,String message){
        String topic = kafkaConfig.getTopicByEvent(eventType);
        log.info("sending message for {} event on topic {} ", eventType.name(),topic);

        kafkaConfig.getTopicByEvent(eventType);
        nodeTemplate.send(topic,message);
    }


}
