package com.enel.workbeat.workbeat.logging.lib;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WorkbeatLoggingLibApplication {

	public static void main(String[] args) {
		SpringApplication.run(WorkbeatLoggingLibApplication.class, args);
	}

}
