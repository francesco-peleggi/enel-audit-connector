package com.enel.workbeat.workbeat.logging.lib.connector.impl;

import com.enel.workbeat.workbeat.logging.lib.connector.EventSender;
import com.enel.workbeat.workbeat.logging.lib.connector.util.WorkbeatAuditConst;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class FileEventProducer implements EventSender {
    @Override
    public void sendEvent(Object event, WorkbeatAuditConst.EventType type) throws IOException {
        String content = new ObjectMapper().writeValueAsString(event);
        String fileName = type.name()+"_audit_";
        BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
        writer.write(content);

        writer.close();
    }
}
