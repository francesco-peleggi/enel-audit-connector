package com.enel.workbeat.workbeat.logging.lib.connector.config;

import com.fasterxml.jackson.databind.ser.std.StringSerializer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import java.util.HashMap;
import java.util.Map;

@Configuration
public class KafkaProducerConfig {

        @Autowired
        private KafkaConfiguration kafkaConfiguration;

        @Bean(name="nodeEventProducerFactory")
        public ProducerFactory<String,String> getProducerFactory(){

                Map<String, Object> configProps = new HashMap<>();
                configProps.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaConfiguration.getBrokerAddress());
                configProps.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
                configProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
                configProps.put(ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG, "true");
                configProps.put(ProducerConfig.CLIENT_ID_CONFIG, kafkaConfiguration.getProducerId());
                configProps.put(ProducerConfig.TRANSACTIONAL_ID_CONFIG, kafkaConfiguration.getGroupId() + kafkaConfiguration.getProducerId());

                return new DefaultKafkaProducerFactory<>(configProps);
        }

        @Bean(name="nodeEventProducer")
        public KafkaTemplate<String,String> getKafkaTemplate0(){
                return  new KafkaTemplate<>(getProducerFactory());
        }

        @Bean(name="taskEventProducer")
        public KafkaTemplate<String,String> getKafkaTemplate1(){
                return  null;
        }

}
