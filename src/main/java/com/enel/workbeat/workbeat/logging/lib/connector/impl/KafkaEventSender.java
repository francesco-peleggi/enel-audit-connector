package com.enel.workbeat.workbeat.logging.lib.connector.impl;

import com.enel.workbeat.workbeat.logging.lib.connector.EventSender;
import com.enel.workbeat.workbeat.logging.lib.connector.util.KafkaProducer;
import com.enel.workbeat.workbeat.logging.lib.connector.util.WorkbeatAuditConst;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.kafka.core.KafkaTemplate;

import java.io.IOException;

public class KafkaEventSender implements EventSender {

    @Autowired
    @Qualifier("nodeEventProducer")
    private KafkaProducer nodeEventProducer;

    @Override
    public void sendEvent(Object event, WorkbeatAuditConst.EventType type) throws IOException {
        nodeEventProducer.sendMessage(type,event.toString());
    }
}
