package com.enel.workbeat.workbeat.logging.lib.connector;

import com.enel.workbeat.workbeat.logging.lib.connector.util.WorkbeatAuditConst;

import java.io.IOException;

public interface EventSender {

    void sendEvent(Object event, WorkbeatAuditConst.EventType type) throws IOException;

}
