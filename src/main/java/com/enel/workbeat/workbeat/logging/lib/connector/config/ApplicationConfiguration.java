package com.enel.workbeat.workbeat.logging.lib.connector.config;

import com.enel.workbeat.workbeat.logging.lib.connector.util.KafkaProducer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConfiguration {

    @Bean(name = "nodeEventProducer")
    public KafkaProducer getNodeEventKafkaProducer(){
        return new KafkaProducer();
    }
}