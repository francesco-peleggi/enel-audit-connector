package com.enel.workbeat.workbeat.logging.lib.connector.config;

import com.enel.workbeat.workbeat.logging.lib.connector.util.WorkbeatAuditConst;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "workbeat.audit.kafka")
public class KafkaConfiguration {

    private String brokerAddress;
    private String producerId;
    private String nodeEventTopic;
    private String taskEventTopic;
    private String processEventTopic;
    private String variableEventTopic;
    private String groupId;

    public String getTopicByEvent(WorkbeatAuditConst.EventType eventType){
        switch(eventType){
            case NODE:       return nodeEventTopic;
            case PROCESS:    return processEventTopic;
            case TASK:       return taskEventTopic;
            case VARIABLE:   return variableEventTopic;
            default: return null;
        }
    }

    public String getProducerConfiguration(WorkbeatAuditConst.EventType eventType){

        StringBuilder stringBuilder = new StringBuilder();
        switch(eventType){
            case NODE:       stringBuilder.append(nodeEventTopic);     break;
            case PROCESS:    stringBuilder.append(processEventTopic);  break;
            case TASK:       stringBuilder.append(taskEventTopic);     break;
            case VARIABLE:   stringBuilder.append(variableEventTopic); break;
        }

        stringBuilder.append("?brokers=").append(this.brokerAddress);
        stringBuilder.append("&clientId=").append(this.producerId);
        stringBuilder.append("&groupId=").append(this.groupId);
        stringBuilder.append("&enableIdempotence=true");
        stringBuilder.append("&maxInFlightRequest=1");
        stringBuilder.append("&retries=5");
        stringBuilder.append("&requestRequiredAcks=all");

        return stringBuilder.toString();
    }

    public String getGroupId(){
        return this.groupId;
    }

    public void setGroupId(String groupId){
        this.groupId=groupId;
    }

    public String getBrokerAddress() {
        return brokerAddress;
    }

    public void setBrokerAddress(String brokerAddress) {
        this.brokerAddress = brokerAddress;
    }

    public String getProducerId() {
        return producerId;
    }

    public void setProducerId(String producerId) {
        this.producerId = producerId;
    }

    public String getNodeEventTopic() {
        return nodeEventTopic;
    }

    public void setNodeEventTopic(String nodeEventTopic) {
        this.nodeEventTopic = nodeEventTopic;
    }

    public String getTaskEventTopic() {
        return taskEventTopic;
    }

    public void setTaskEventTopic(String taskEventTopic) {
        this.taskEventTopic = taskEventTopic;
    }

    public String getProcessEventTopic() {
        return processEventTopic;
    }

    public void setProcessEventTopic(String instanceEventTopic) {
        this.processEventTopic = instanceEventTopic;
    }

    public String getVariableEventTopic() {
        return variableEventTopic;
    }

    public void setVariableEventTopic(String variableEventTopic) {
        this.variableEventTopic = variableEventTopic;
    }



}
